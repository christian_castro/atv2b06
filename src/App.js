import './App.css';
import { useState } from "react"
import { GlobalStyle } from './components/GlobalStyle';
import { Routes } from './components/Routes';



const App = () => {

  const [logado, setLogado] = useState(false)

  return (
    <div className="App">
      <header className="App-header">
        <GlobalStyle />
        <Routes logado={logado} setLogado={setLogado} />
      </header>
    </div>
  );
}

export default App;
