import { useForm } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from "yup"
import { useHistory } from "react-router-dom"
import { TextField, Button } from '@material-ui/core';
import { FormInput } from "./styles"
import { motion } from 'framer-motion'
import SaveIcon from "@material-ui/icons/Save"
import '@fontsource/roboto'


export const Form = ({ setLogado, icon: Icon }) => {

    const history = useHistory()

    const formSchema = yup.object().shape({
        nome: yup.string().min(2).required('Nome obrigatório').matches(/^[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ ]+$/, 'É Obrigatório que só tenha letras.'),

        email: yup.string().email().required('Email obrigatório'),

        senha: yup.string().min(8, 'minímo de 8 caracteres').required('Senha obrigatório'),

        confirmarSenha: yup.string().oneOf([yup.ref("senha")], 'As senhas precisam ser iguais.').matches(
            /^((?=.*[!@#$%^&*()\-_=+{};:,<.>]){1})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/,
            "Precisa pelo menos uma letra maiúscula, minúscula e um caracter especial"
          ).required('A confirmação de senha é obrigatória.'),    
    })

    const { register , handleSubmit, formState: { errors } } = useForm({
        resolver: yupResolver(formSchema)
    })

    const onSubmit = (data) => {
        if(data) {
            history.push(`/logado/${data.nome}`)
            setLogado(true)
        } 
    }
    return (
        <motion.div 
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        exit={{ opacity: 0 }}
        transition={{ duration: 0.7 }}
        >
        <div>
            <h1>Cadastro Kenzie Academy</h1>

            <FormInput onSubmit={handleSubmit(onSubmit)}>

                <TextField label="Nome" {...register('nome')} />
                <h5>{errors.nome && errors.nome.message}</h5>

                <TextField  label="E-mail" {...register('email')} />
                <h5>{errors.email && errors.email.message}</h5>

                <TextField label="Senha" type="password" {...register('senha')} />
                <h5>{errors.senha && errors.senha.message}</h5>

                <TextField label="Confirmar Senha" type="password" {...register('confirmarSenha')} />
                <h5>{errors.confirmarSenha && errors.confirmarSenha.message}</h5>

                <div>
                    <Button endIcon={<SaveIcon />} variant="outlined" color="secondary" type="submit">Cadastrar</Button>
                </div>
            </FormInput>
       </div>
       </motion.div>
    )
}