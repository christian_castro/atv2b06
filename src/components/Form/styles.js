import styled from "styled-components";


export const FormInput = styled.form`
  display: flex;
  flex-direction: column;
  max-width: 750px;
  background: #7F7FD5;  
  background: -webkit-linear-gradient(to bottom, #91EAE4, #d2d9e6, #dadae6); 
  background: linear-gradient(to bottom, #91EAE4, #d6d9df, #e3e3e6); 
  width: 20rem;
  padding: 1.2rem;
  margin: 1.5rem auto;
  border-radius: 1.2rem;

  input {
    margin: 7px 0 7px 0;
    border-radius: 4px;
  }

  button {
    width: 250px;
    cursor: pointer;
    height: 40px;
    background-color: #fafcfd;
    border: 0;
    margin-top: 1rem;
    color: #6735da;
    border: 2px solid #6735da;;
    font-weight: bold;
  }

  h5 {
    color: #e94242;
  }
`;
