import { useHistory, useParams } from "react-router-dom"
import Trophy from "../../assets/trophy.png"
import "./styles.css"
import { Button } from "@material-ui/core"
import { motion } from 'framer-motion'

export const Logado = ({ setLogado }) => {

    const history = useHistory()
    const params = useParams()

    const handleLogout = () => {
        setLogado(false)
        history.push("/")
    }

    return (
        <motion.div 
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        exit={{ opacity: 0 }}
        transition={{ duration: 0.9 }}
        >
         <div className="container">
            <h1>Seja Bem-Vindo(a) {params.id}</h1>
            <img src={Trophy} alt={Trophy} className="img"/>
            <div className="btn-logout">
              <Button variant="outlined" color="secondary" size="large" onClick={handleLogout} style={{marginTop: "2rem", background: "#fff"}}>Deslogar</Button>
            </div>
          </div>
        </motion.div>
    )
}