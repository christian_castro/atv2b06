import { Form } from "../Form"
import { Logado } from "../PageLogado"
import { PageNotFound } from "../PageNotFound"
import { Switch, Route } from "react-router-dom"

export const Routes = ({ logado, setLogado }) => {
    return (
        <div>
            <Switch>

            {
            !logado ? <Route exact path="/"> <Form setLogado={setLogado}/>  </Route> 
            :
            <Route path="/logado/:id"> <Logado setLogado={setLogado} /> </Route>
            }

            <Route path="*">  <PageNotFound /> </Route>
            
            </Switch>
        </div>
    )
}