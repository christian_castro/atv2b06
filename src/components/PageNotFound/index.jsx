import { useHistory } from 'react-router-dom'
import "./styles.css"
import { Button } from "@material-ui/core"
import { motion } from 'framer-motion'
import Sad from "../../assets/sad.png"


export const PageNotFound = () => {

    const StyleButtonHome = {
        width: "13rem", height: "2.7rem", background: "#2020df54", color: "#fff", fontWeight: "bold"
    }

    const history = useHistory()

    return (
       <div className="containerAll">
          <motion.div 
            initial={{ opacity: 0 }}
            animate={{ opacity: 1 }}
           exit={{ opacity: 0 }}
           transition={{ duration: 0.7 }}>
           <div className="container-notFound">
              <h1>ERROR 404. PÁGINA NÃO ENCONTRADA!</h1>  
              <img src={Sad} alt={Sad} className="sadImage" />
              <div>
                 <br />
                 <Button variant="outlined" size="large" onClick={() => history.push("/")} style={StyleButtonHome}>Home</Button>
              </div>
           </div>
         </motion.div>
       </div>
    )
}